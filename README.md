## rulisastra's Notebook
Ini adalah Latihan dan tugas
BACKEND || DTS - TA Digitalent
3 November 2020
>>> ### 24. Implementasi MVC + Go Lang

# How to use
ada 2 bagian.
> ini bagian 2
bagian 1
> https://gitlab.com/rulisastra/24_mvc.git

## install from template MVC (model | views | controller)
https://github.com/FadhlanHawali/Digitalent-Kominfo_Implementation-MVC-Golang.git

### install Bagian 1
```
go get -u gorm.io/gorm
go get -u gorm.io/driver/mysql
go get -u github.com/gin-gonic/gin
go get -u github.com/gin-contrib/cors
```

### install Bagian 2
```
go get golang.org/x/crypto/bcrypt
```
### install middleware
```
go get github.com/dgrijalva/jwt-go 
go get github.com/mitchellh/mapstructure 

```
## happy coding
1. edit app/config.go
ambil codenya di branch ke dua fadlanhawaii

2. start XAMPP (Apache dan Mysql)
masuk mysql dengan
```mysql -u root```

3. bikin database dan table
Bikin database
```
CREATE DATABASE IF NOT EXISTS `digitalent_bank`
```
## > BAGIAN 1 akan secara oromatis membuat tabel

Manual Bikin Tabel Account (not needed)
```
CREATE TABLE IF NOT EXISTS `account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_account` varchar(50) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `account_number` int(11) DEFAULT NULL,
  `saldo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
```
Manual Bikin Tabel Transaction (not needed)
```
CREATE TABLE IF NOT EXISTS `transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_account_refer` int(11) DEFAULT NULL,
  `id_transaction` varchar(50) DEFAULT NULL,
  `transaction_type` int(11) DEFAULT NULL,
  `transaction_description` varchar(50) DEFAULT NULL,
  `sender` int(11) DEFAULT NULL,
  `recipient` int(11) DEFAULT NULL,
  `timestamp` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

```

4. ### ERROR 
>>> DB undefined

solved:
tambahin file config.go di folder /model/

>>> root mysql
kalau ngga pake password
```root:@/digitalent_bank```
kalau dari template
```root:root@/digitalent_bank?charset=utf8&parseTime=True&loc=Local```

solved:
karena aku lupa password xampp/mysql/root

maka akses http://localhost/phpmyadmin/ setelah start apache di xampp

sumber mengubah password = '' menjadi kosong

https://gist.github.com/susanBuck/39d1a384779f3d596afb19fcad6b598c

### delete folder /app/config && /app/handler

# SOLVED : 
harus di ```go run main.go``` untuk setiap bagian. jangan langsung ke 2

```
func DBInit() *gorm.DB {
	db, err := gorm.Open(mysql.Open(fmt.Sprintf("root@/digitalent_bank?charset=utf8&parseTime=True&loc=Local")), &gorm.Config{})
	if err != nil {
		panic("failed to connect to database" + err.Error())
	}

	db.AutoMigrate(new(model.Account), new(model.Transaction))
	return db
}
```

5. running
``` go run main.go ```
